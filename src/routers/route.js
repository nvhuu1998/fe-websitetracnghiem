import React, { Fragment } from 'react';
import { Route, Switch } from 'react-router-dom'
import detailSubject from '../pages/Subject/detail';
import detailUser from './../pages/User/detail';
import detailPart from './../pages/Part/detail';
import Login from './../pages/Login/login';
import DetailQues from './../pages/Question/detail';
import DetailTopic from './../pages/Topic/detail';
import Public from './../pages/Public/public';
import DetailExam from './../pages/Exam/detail';
import DetailHistory from './../pages/History/detail';
import DetailDoTest from './../pages/DoTest/detail';
import DetailHistoryTest from './../pages/DoTest/historytest';
import Home from './../pages/home';

const routes = () => {
  return (
    <Switch>
      <Fragment>
        <Route path='/public' exact component={Public} />
        <Route path='/home/:id' exact component={Home} />
        <Route path='/' exact component={detailUser} />
        <Route path='/user' exact component={detailUser} />
        <Route path='/subject' exact component={detailSubject} />
        <Route path='/part' exact component={detailPart} />
        <Route path='/topic' exact component={DetailTopic} />
        <Route path='/question' exact component={DetailQues} />
        <Route path='/login' exact component={Login} />
        <Route path='/exam' exact component={DetailExam} />
        <Route path='/history' exact component={DetailHistory} />
        <Route path='/dotest/:id' exact component={DetailDoTest} />
        <Route path='/historytest' exact component={DetailHistoryTest} />
      </Fragment>
    </Switch >)
}
export default routes;