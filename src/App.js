import { Layout } from 'antd';
import FooterPage from './components/Footer';
import HeaderPage from './components/Header';
import MenuPage from './components/Menu';
import Routes from './routers/route';
import { useHistory } from "react-router";
import './style/index.scss';
import { useLocation } from 'react-router-dom'
import { useEffect } from 'react';
import HeaderPublic from './components/HeaderPublic';
const { Content } = Layout;

const App = () => {
  const history = useHistory();
  const location = useLocation()
  const isLogin = localStorage.getItem('auth');
  useEffect(() => {
    if (!isLogin) history.push('/login')
  }, [])
  if (location.pathname.includes('/login')) {
    return (
      <Content>
        <Routes />
      </Content>
    )
  } else
    if (location.pathname.includes('/public') || location.pathname.includes('/dotest') || location.pathname.includes('/historytest')
      || location.pathname.includes('/home')) {
      return (
        <Layout className='site-layout'>
          <HeaderPublic />
          <Content className='content-public' style={{ minHeight: '80vh', margin: '16px 16px' }}>
            <Routes />
          </Content>
          <FooterPage />
        </Layout>
      )
    } else
      return (
        <Layout style={{ minHeight: "100vh" }}>
          <MenuPage />
          <Layout>
            <HeaderPage />
            <Content style={{ margin: '16px 16px' }}>
              <div className="content-background" style={{ padding: 24 }} >
                <Routes />
              </div>
            </Content>
            <FooterPage />
          </Layout>
        </Layout >
      );
}

export default App;
