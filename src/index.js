import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import App from "./App";
import store from "./redux/store";
import { Provider } from "react-redux";
import axios from "axios";
import { createBrowserHistory } from 'history'
import { Router } from 'react-router'
import { BrowserRouter } from 'react-router-dom'
axios.defaults.baseURL = "http://localhost:5000/api";
const history = createBrowserHistory()
ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Router>
  </Provider>
  ,
  document.getElementById("root")
);
