import { TestType } from "../actions/AuthAction";
const idExam = ''
const getIdExam = (state = idExam, action) => {
  switch (action.type) {
    case TestType.IDTEST_SUCCESS:
      const newID = action.payload
      return newID
    default:
      return state
  }
}
export default getIdExam