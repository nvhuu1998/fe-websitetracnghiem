import React from 'react';
import 'antd/dist/antd.css';
import { Dropdown, Menu, PageHeader, Button } from "antd";
import { connect } from "react-redux";
import { UserOutlined, HomeOutlined, LogoutOutlined } from '@ant-design/icons'
import { AuthActionType } from '../redux/actions/AuthAction';

import { useDispatch } from 'react-redux'
import { useHistory } from "react-router";
const HeaderPublic = (props) => {
  const dispatch = useDispatch()
  const history = useHistory();
  const menu = (
    <Menu>
      <Menu.Item key='1' icon={<HomeOutlined />} onClickCapture={() => history.push('/')}>
        Quản lý
            </Menu.Item>
      <Menu.Item key='3' icon={<LogoutOutlined />}
        onClickCapture={() => logout()}>
        Đăng xuất
            </Menu.Item>
    </Menu>
  )
  const logout = () => {
    dispatch({
      type: AuthActionType.LOGOUT_SUCCESS,
      payload: "logout",
    });
    history.push("/login");
  }
  return (
    <PageHeader
      className='site-page-header'
      title='Website hỗ trợ ôn thi đại học'
      extra={[
        <Button key="3" onClick={() => { history.push('/historytest') }}>Lịch sử thi</Button>,
        <Button key="2" style={{ marginRight: '10px' }} onClick={() => { history.push('/public') }}
        >Làm bài thi</Button>,
        <Dropdown.Button overlay={menu} icon={<UserOutlined key='1' />} style={{ width: 200 }}>
          {props.user.name}
        </Dropdown.Button>
      ]}
    ></PageHeader>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.authState.user.user,
  };
};

export default connect(mapStateToProps)(HeaderPublic);