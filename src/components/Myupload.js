import Axios from 'axios';
class MyUploadAdapter {
  constructor(loader) {
    // The file loader instance to use during the upload.
    this.loader = loader;
  }

  // Starts the upload process.
  upload() {
    return this.loader.file
      .then(file => new Promise((resolve, reject) => {

        const toBase64 = file => new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => resolve(reader.result);
          reader.onerror = error => reject(error);
        });

        return toBase64(file).then(cFile => {
          return Axios.post("/img", { file: file }).then((d) => {
            if (d.status) {
              this.loader.uploaded = true;
              resolve({
                default: d.response.url
              });
            } else {
              reject(`Couldn't upload file: ${file}.`)
            }
          }).catch((err) => {
            console.log(err.response.data)
          });
        })

      }));
  }


}

// ...

export default function MyCustomUploadAdapterPlugin(editor) {
  editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
    // Configure the URL to the upload script in your back-end here!
    return new MyUploadAdapter(loader);
  };
}