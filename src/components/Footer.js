import { Footer } from "antd/lib/layout/layout";
import { React } from 'react';
const FooterPage = () => {
  return (
    <Footer style={{ textAlign: 'center' }}>Nguyễn Văn Hữu</Footer>
  )
}
export default FooterPage;