import React from 'react';
import 'antd/dist/antd.css';
import { Dropdown, Menu, PageHeader } from "antd";
import { connect } from "react-redux";
import { UserOutlined, HomeOutlined, LogoutOutlined } from '@ant-design/icons'
import { AuthActionType } from '../redux/actions/AuthAction';

import { useDispatch } from 'react-redux'
import { useHistory } from "react-router";
const HeaderPage = (props) => {
  const dispatch = useDispatch()
  const history = useHistory();
  const menu = (
    <Menu>
      <Menu.Item key='1' icon={<HomeOutlined />} onClickCapture={() => history.push('/public')}>
        Trang chủ
            </Menu.Item>
      <Menu.Item key='3' icon={<LogoutOutlined />}
        onClickCapture={() => logout()}>
        Đăng xuất
            </Menu.Item>
    </Menu>
  )
  const logout = () => {
    dispatch({
      type: AuthActionType.LOGOUT_SUCCESS,
      payload: "logout",
    });
    history.push("/login");
  }
  return (
    <PageHeader
      className='site-page-header'
      title='Website hỗ trợ ôn thi đại học'
      extra={[
        <Dropdown.Button overlay={menu} icon={<UserOutlined />} style={{ width: 200 }}>
          {props.user?.name}
        </Dropdown.Button>
      ]}
    ></PageHeader>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.authState.user.user,
  };
};

export default connect(mapStateToProps)(HeaderPage);