import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { Layout, Menu } from 'antd';
import {
  DesktopOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { useHistory, useLocation } from 'react-router-dom'

const { Sider } = Layout;
const { SubMenu } = Menu;

const MenuPage = () => {
  const history = useHistory()
  const location = useLocation()
  const initialMenuState = {
    menuKey: ['1'],
    parentMenuKey: [''],
    currentMenuKey: [''],
    userDetail: null
  }

  const locationState = (location && (location.state)) || initialMenuState
  const [state, setState] = useState({
    currentMenuKey: locationState.menuKey,
    parentMenuKey: locationState.parentMenuKey,
    userDetail: null
  })
  const navigatePage = (menuKey, parentMenuKey, link) => {
    history.push(link, { menuKey, parentMenuKey })
  }
  return (
    <Sider collapsible >
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
        <Menu.Item key="1" icon={<PieChartOutlined />}>
          <Link to='/'>Tổng quan</Link>
        </Menu.Item>
        <Menu.Item key="2" icon={<PieChartOutlined />}>
          <Link to='/user'>Quản lý người dùng</Link>
        </Menu.Item>
        <SubMenu key="sub1" icon={<UserOutlined />} title="Quản lý câu hỏi">
          <Menu.Item key="3" icon={<DesktopOutlined />}>
            <Link to='/subject'>Môn</Link>
          </Menu.Item>
          <Menu.Item key="4" icon={<DesktopOutlined />}>
            <Link to='/part'>Dạng</Link>
          </Menu.Item>
          <Menu.Item key="5" icon={<DesktopOutlined />}>
            <Link to='/topic'>Chủ đề</Link>
          </Menu.Item>
          <Menu.Item key="6" icon={<DesktopOutlined />}>
            <Link to='/question'>Câu hỏi</Link>
          </Menu.Item>
        </SubMenu>
        {/* <SubMenu key="sub2" icon={<UserOutlined />} title="Quản lý đề thi"> */}
        <Menu.Item key="7" icon={<DesktopOutlined />}>
          <Link to='/exam'>Đề thi</Link></Menu.Item>
        {/* <Menu.Item key="8">Đề thi chính thức</Menu.Item>
          <Menu.Item key="9">Alex</Menu.Item>
        </SubMenu> */}
        {/* <SubMenu key="sub3" icon={<TeamOutlined />} title="Thống kê"> */}
        <Menu.Item key="10" icon={<DesktopOutlined />}><Link to='/history'>Lịch sử thi</Link></Menu.Item>
        {/* <Menu.Item key="11">Kỷ lục</Menu.Item>
        </SubMenu> */}
        {/* <Menu.Item key="12" icon={<FileOutlined />}>
          <Link to='/me'>Thông tin tải khoản</Link>
        </Menu.Item> */}
      </Menu>
    </Sider >)
}
export default MenuPage;