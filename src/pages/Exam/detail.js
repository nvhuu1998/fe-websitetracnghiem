import React, { useEffect, useState } from 'react';
import { Button, Col, Drawer, notification, Popconfirm, Row, Space, Table, Form, Input, Select, Radio, Tree } from 'antd';
import Search from 'antd/lib/input/Search';
import Axios from 'axios';
import { connect } from "react-redux";
import dateFormat from 'dateformat'

const DetailExam = (props) => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [total, setTotal] = useState();
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState('');
  const [perPage, setPerPage] = useState(10);
  const [visible, setVisibale] = useState(false);
  const [error, setError] = useState([]);
  const [loadPage, setLoadPage] = useState(true)
  const [dataEdit, setDataEdit] = useState();
  const [title, setTitle] = useState('')
  const [isEdit, setIsEdit] = useState(true)
  const [action, setAction] = useState('')
  const [listSubject, setListSubject] = useState([])
  const [type, setType] = useState(1)
  const [idSubject, setIdsubject] = useState('')
  const getTreeData = async () => {
    try {
      const result = await Axios.get(`/part/subject/${idSubject}`)
      const tree = result.data?.data
      let treeDataa = []
      tree.map(part => {
        let child = []
        part.topics.map(topic => {
          child.push({ title: topic.name, key: topic._id })
        })
        treeDataa.push({ title: part.name, key: part._id, children: child })
      })
      console.log(treeDataa)
      setTreeData(treeDataa)
    } catch (error) {

    }
  }
  const changeSubject = (value) => {
    setIdsubject(value)
    getTreeData()
  }
  const [treeData, setTreeData] = useState([])
  const [expandedKeys, setExpandedKeys] = useState([]);
  const [checkedKeys, setCheckedKeys] = useState([]);
  const [selectedKeys, setSelectedKeys] = useState([]);
  const [autoExpandParent, setAutoExpandParent] = useState(true);

  const onExpand = (expandedKeys) => {
    setExpandedKeys(expandedKeys);
    setAutoExpandParent(false);
  };

  const onCheck = (checkedKeys) => {
    setCheckedKeys(checkedKeys);
  };

  const onSelect = (selectedKeys, info) => {
    setSelectedKeys(selectedKeys);
  };
  const columns = [
    {
      title: 'STT',
      dataIndex: 'key',
      key: 'key',
      render: (_, record) => (
        <Space>{(page - 1) * 10 + (data.indexOf(record) + 1)}</Space>
      )
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'date_create',
      key: 'date_create',
      render: (_, record) => (
        <Space>{dateFormat(record?.date_create, 'h:MM:ssTT dd/mm/yyyy')}</Space>
      )
    },
    {
      title: 'Tên đề thi',
      dataIndex: 'name',
      key: 'key',
    },
    {
      title: 'Môn',
      key: 'key',
      render: (_, record) => (
        <Space>{record?.subject_id?.name}</Space>
      )
    },
    {
      title: 'Số câu hỏi',
      key: 'key',
      render: (_, record) => (
        <Space>{record?.ques?.length}</Space>
      )
    },
    {
      title: 'Chức năng',
      key: 'key',
      render: (_, record) => (
        <Space>
          <Popconfirm
            title='Xóa đề thi này'
            okText='Xóa'
            cancelText='Hủy'
            onConfirm={() => deleteRecord(record._id)}>
            <Button type='primary' danger >Xóa</Button>
          </Popconfirm>
        </Space>
      )
    },
  ];


  const getData = async () => {
    let result;
    try {
      result = await Axios.post('/exam', { search_text: search, page: page || 1, perPage: perPage })

      setData(result?.data?.data);
      setTotal(result?.data.total);
      const result2 = await Axios.get('subject')
      setListSubject(result2.data?.data)
      setIdsubject(result2?.data.data[0]._id)
      getTreeData()
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
    setLoading(false);
  }
  useEffect(() => {

    if (loading) {
      getData();
      setLoadPage(false);
    }
  }, [loading]);

  const handleChange = (pagination) => {
    setPage(pagination.current)
    setPerPage(pagination.pageSize)
    setLoading(true)
  }
  const onSearch = value => {
    setSearch(value);
    setPage(1)
    setLoading(true);
  }
  const closeModal = () => {
    setError([])
    setVisibale(false)
    setDataEdit([])
    setIsEdit(true)
  }
  const onCreate = async () => {
    let data
    try {
      data = await form.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    console.log(data)
    let formData
    // if (data.type) setError({ type: "Chọn loại đề thi" })
    // else
    if (data.type === 1) {
      formData = {
        subject_id: data.subject_id
      }
    } else {
      formData = {
        subject_id: data.subject_id,
        topics: checkedKeys
      }
    }
    try {
      let result
      if (data.type === 1)
        result = await Axios.post('/exam/add', formData)
      else
        result = await Axios.post('/exam/add/topic', formData)
      if (result.status == 201) {
        setVisibale(false)
        setLoading(true)
        form.resetFields()
        setError([])
        notification['success']({
          message: 'Thành công!',
          description: 'Thêm mới đề thi thành công!'
        })
      }
    } catch (err) {
      if (!err.response?.data) notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
      else
        setError(err.response?.data)
    }
  }
  const deleteRecord = async (id) => {
    try {
      const result = await Axios.delete(`/exam/${id}`);
      setLoading(true)
      notification['success']({
        message: 'Thành công!',
        description: 'Xóa thành công!'
      })
    } catch (err) {
      notification['error']({
        message: 'Thất bại!',
        description: err.response?.data
      })
    }
  }
  const [form] = Form.useForm()


  return loadPage ? "" : (
    <div>
      <Row justify='space-between'>
        <Col><h2>Danh đề thi</h2></Col>
        <Col><Search
          placeholder="Nhập từ khóa"
          allowClear
          enterButton="Tìm kiếm"
          onSearch={onSearch}
        /></Col>
        <Col> <Button type='primary' onClick={() => { setVisibale(true); setTitle('Thêm mới'); setAction('Add'); form.resetFields() }}>
          Thêm mới</Button>
        </Col>
      </Row>
      <br />
      <Table columns={columns}
        dataSource={data}
        size={'small'}
        rowKey='id'
        loading={loading}
        pagination={{ pageSize: perPage, current: page, total: total }}
        onChange={handleChange} />
      <Drawer
        title={title}
        width={500}
        onClose={closeModal}
        visible={visible}
        footer={
          <div
            style={{
              textAlign: 'left',
            }}
          >
            <Button onClick={closeModal} style={{ marginRight: 8 }}>
              Hủy
            </Button>

            <Button onClick={() => onCreate()} type="primary">
              Thêm
              </Button>

          </div>
        }
      >
        <Form
          form={form}
          layout="vertical"
        >
          <Form.Item
            name='subject_id'
            label='Tên Môn'
            validateStatus={error?.subject_id ? "error" : ""}
            help={error?.subject_id ? error?.subject_id : ""}
          >
            <Select placeholder='Chọn môn' onChange={changeSubject} value={idSubject}>
              {listSubject.map((subject) =>
                <Select.Option value={subject._id} key={subject._id}>{subject.name}</Select.Option>)}
            </Select>
          </Form.Item>

          <Form.Item
            name='type'
            label='Chọn dạng đề thi'
            validateStatus={error?.name ? "error" : ""}
            help={error?.name ? error?.name : ""}
          >
            <Radio.Group buttonStyle="solid" onChange={(e) => setType(e.target.value)}>
              <Space>
                <Radio.Button value={1}>Trộn đề ngẫu nhiên</Radio.Button>
                <Radio.Button value={2}>Trộn đề tự chọn</Radio.Button>
              </Space>
            </Radio.Group>
          </Form.Item>
          {type === 2 ? <Form.Item
            // name='subject_id'
            label='Chọn chủ đề'
            validateStatus={error?.subject_id ? "error" : ""}
            help={error?.subject_id ? error?.subject_id : ""}
          >
            <Tree
              checkable
              onExpand={onExpand}
              expandedKeys={expandedKeys}
              autoExpandParent={autoExpandParent}
              onCheck={onCheck}
              checkedKeys={checkedKeys}
              onSelect={onSelect}
              selectedKeys={selectedKeys}
              treeData={treeData}
            />
          </Form.Item> : ''}
        </Form>
      </Drawer>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.authState.user.user,
  };
};

export default connect(mapStateToProps,)(DetailExam);