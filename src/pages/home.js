import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { Col, Row, notification, Button, Radio, Result, Modal } from 'antd';
import HtmlParser from 'react-html-parser';
import { useHistory } from "react-router";
import dateFormat from 'dateformat'
const radioStyle = {
  display: 'block',

};
const Home = (props) => {
  const history = useHistory();
  const [data, setData] = useState()
  const [exam, setExam] = useState()
  const [loading, setLoading] = useState(true)
  const getData = async (id) => {
    let result;
    try {
      result = await Axios.get(`/test/${id}`)
      setData(result?.data?.data)
      getExam(result?.data?.data?.exam_id)
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
    setLoading(false);
  }
  const getExam = async (id) => {

    try {
      const result = await Axios.get(`/exam/${id}`)
      setExam(result?.data?.data)
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
  }
  useEffect(() => {
    const id = props.match.params.id
    if (loading) {
      getData(id)
    }

  }, [])
  console.log(data)
  console.log(exam)

  return loading ? '' : (
    <Row align='middle' justify='center' style={{ minHeight: '80vh', margin: '20px' }} gutter={16}>
      <Col span={18} offset={2}>
        {exam?.ques.map((ques, index) => (<>
          <Row key={index}>
            <strong> Câu {index + 1}:</strong>{HtmlParser(ques.question_id.question)}
          </Row>
          {ques.question_id.image ? <Row><image src={`${URL}${ques.question_id.image}`} /> </Row> : ''}
          < Row >
            <Radio.Group disabled defaultValue={data?.answers[index]}>
              <Radio style={radioStyle} value={ques?.question_id?.answers[0]?._id} >
                A. <span style={{ display: 'inline-block' }}> {HtmlParser(ques.question_id.answers[0]?.ans)}</span>
              </Radio>

              <Radio style={radioStyle} value={ques.question_id?.answers[1]?._id} >
                B. <span style={{ display: 'inline-block' }}>  {HtmlParser(ques.question_id.answers[1]?.ans)}</span>
              </Radio>
              <Radio style={radioStyle} value={ques.question_id?.answers[2]?._id} >
                C. <span style={{ display: 'inline-block' }}>  {HtmlParser(ques.question_id.answers[2]?.ans)}</span>
              </Radio>
              <Radio style={radioStyle} value={ques.question_id?.answers[3]?._id} >
                D. <span style={{ display: 'inline-block' }}>  {HtmlParser(ques.question_id.answers[3]?.ans)}</span>
              </Radio>
            </Radio.Group>

          </Row>
          <Row><strong> Đáp án đúng: {ques.question_id.answers.map((ans, index) => {
            if (ans.isTrue === true) return <span style={{ display: 'inline-block' }
            }> {HtmlParser(ans.ans)}</span>
          })}</strong></Row>
        </>)
        )}
        <Row>
          <Col offset={10}>
            <Button type='primary' onClick={() => history.push('/historytest')}>Quay lại</Button>
          </Col>
        </Row>

      </Col>

    </Row >)
}
export default Home;