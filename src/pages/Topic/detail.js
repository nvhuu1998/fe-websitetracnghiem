import React, { useEffect, useState } from 'react';
import { Button, Col, Drawer, notification, Popconfirm, Row, Space, Table, Form, Input, Select } from 'antd';
import Axios from 'axios';
import { connect } from "react-redux";

const DetailTopic = (props) => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [total, setTotal] = useState();
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [visible, setVisibale] = useState(false);
  const [error, setError] = useState([]);
  const [loadPage, setLoadPage] = useState(true)
  const [dataEdit, setDataEdit] = useState();
  const [title, setTitle] = useState('')
  const [idSubject, setIdSubject] = useState('')
  const [idPart, setIdPart] = useState('')
  const [action, setAction] = useState('')
  const [listSubject, setListSubject] = useState([])
  const [listPart, setListPart] = useState([])
  const columns = [
    {
      title: 'STT',
      dataIndex: 'key',
      key: 'key',
      width: 100,
      render: (_, record) => (
        <Space>{(page - 1) * 10 + (data.indexOf(record) + 1)}</Space>
      )
    },
    {
      title: 'Tên chủ đề',
      dataIndex: 'name',
      key: 'key',
    },
    {
      title: 'Tên Môn',
      dataIndex: 'nameSubject',
      key: 'key',
      render: (_, record) => <>{record?.subject_id?.name}</>
    },
    {
      title: 'Tên Dạng',
      dataIndex: 'namePart',
      key: 'key',
      render: (_, record) => <>{record?.part_id?.name}</>
    },
    // {
    //   title: 'Số câu hỏi',
    //   dataIndex: 'countQues',
    //   key: 'countQues',
    // },
    {
      title: 'Chức năng',
      key: 'key',
      render: (_, record) => (
        <Space>
          <Button type='primary' onClick={() => onClickEdit(record)}>
            Cập nhật</Button>
          <Popconfirm
            title='Xóa Chủ đề này'
            okText='Xóa'
            cancelText='Hủy'
            onConfirm={() => deleteRecord(record._id)}>
            <Button type='primary' danger >Xóa</Button>
          </Popconfirm>
        </Space>
      )
    },
  ];

  const getData = async () => {
    let result;
    try {
      if (idPart !== '') {
        result = await Axios.post(`/topic/part/${idPart}`, { page: page || 1, perPage: perPage })
      }
      else if (idSubject !== '') {
        result = await Axios.post(`/topic/subject/${idSubject}`, { page: page || 1, perPage: perPage })
      }
      else {
        result = await Axios.post('/topic', { page: page || 1, perPage: perPage })
        const result3 = await Axios.get('/part')
        setListPart(result3?.data?.data)
      }
      setData(result?.data?.data);
      setTotal(result?.data.total);
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
    let result2;
    try {
      result2 = await Axios.post('/subject', { page: 1, perPage: 100 })
      setListSubject(result2?.data?.data)
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }

    setLoading(false);
  }
  useEffect(() => {
    if (loading) {
      getData();
      setLoadPage(false);
    }
  }, [loading]);

  const handleChange = (pagination) => {
    setPage(pagination.current)
    setPerPage(pagination.pageSize)
    setLoading(true)
  }

  const closeModal = () => {
    setError([])
    setVisibale(false)
    setDataEdit([])
  }
  const onClickEdit = (record) => {
    setVisibale(true);
    setAction('Cập nhật');
    setDataEdit(record);
    setTitle('Cập nhật');
    form.setFieldsValue({ name: record.name, subject_id: record.subject_id?._id, part_id: record.part_id?._id })
    changeSubject(record.subject_id?._id)
  }
  const onCreate = async () => {
    let data
    try {
      data = await form.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    data.creator = props.user.id
    try {
      const result = await Axios.post('/topic/add', data)
      if (result.status === 201) {
        setVisibale(false)
        setLoading(true)
        form.resetFields()
        setError([])
        notification['success']({
          message: 'Thành công!',
          description: 'Thêm mới chủ đề thành công!'
        })
      }
    } catch (err) {
      if (!err.response?.data) notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
      else
        setError(err.response?.data)
    }
  }
  const deleteRecord = async (id) => {
    try {
      const result = await Axios.delete(`/topic/${id}`);
      setLoading(true)
      notification['success']({
        message: 'Thành công!',
        description: 'Xóa thành công!'
      })
    } catch (err) {
      notification['error']({
        message: 'Thất bại!',
        description: err.response?.data
      })
    }
  }
  const [form] = Form.useForm()
  const onEdit = async () => {
    let data
    const id = dataEdit._id;
    try {
      data = await form.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    data.creator = props.user.id
    try {
      await Axios.patch(`/topic/${id}`, data)
      setVisibale(false)
      setLoading(true)
      form.resetFields()
      setError([])
      notification['success']({
        message: 'Thành công!',
        description: 'Cập nhật Môn thành công!'
      })
    } catch (err) {
      if (!err.response?.data) notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
      else
        setError(err.response?.data)
    }
  }
  const changeSubject = async (value) => {
    let result;
    try {
      if (value !== '') {
        result = await Axios.get(`/part/subject/${value}`)
      }
      else {
        result = await Axios.get(`/part`)
      }
      setListPart(result?.data?.data);
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
  }

  const handleSubject = (value) => {
    setIdSubject(value)
    setIdPart('')
    changeSubject(value)
    setLoading(true)
    setPage(1)
  }
  const handlePart = (value) => {
    setIdPart(value)
    setPage(1)
    setLoading(true)
  }
  return loadPage ? "" : (
    <div>
      <Row justify='space-between'>
        <Col><h2>Danh sách chủ đề</h2></Col>
        <Col ><Space>
          Môn:
          <Select defaultValue={idSubject} style={{ width: 200 }} onChange={handleSubject}>
            <Select.Option value=''>Tất cả</Select.Option>
            {listSubject.map((subject) =>
              <Select.Option value={subject._id} key={subject._id}>{subject.name}</Select.Option>)}
          </Select>
        </Space>
        </Col>
        <Col ><Space>
          Dạng:
          <Select value={idPart} style={{ width: 200 }} onChange={handlePart}>
            <Select.Option value=''>Tất cả</Select.Option>
            {listPart.map((part) =>
              <Select.Option value={part._id} key={part._id}>{part.name}</Select.Option>)}
          </Select>
        </Space>
        </Col>
        <Col> <Button type='primary' onClick={() => { setVisibale(true); setTitle('Thêm mới'); setAction('Add'); form.resetFields() }}>
          Thêm mới</Button>
        </Col>
      </Row>
      <br />
      <Table columns={columns}
        dataSource={data}
        size={'small'}
        rowKey='id'
        loading={loading}
        pagination={{ pageSize: perPage, current: page, total: total }}
        onChange={handleChange} />

      <Drawer
        title={title}
        width={300}
        onClose={closeModal}
        visible={visible}
        footer={
          <div
            style={{
              textAlign: 'left',
            }}
          >
            <Button onClick={closeModal} style={{ marginRight: 8 }}>
              Hủy
            </Button>
            {action === 'Add' ?
              <Button onClick={() => onCreate()} type="primary">
                Thêm
              </Button> : <Button onClick={() => onEdit()} type="primary">
                Cập nhật
              </Button>}

          </div>
        }
      >
        <Form
          form={form}
          layout="vertical"
        >
          <Form.Item
            name='subject_id'
            label='Tên Môn'
            validateStatus={error?.subject_id ? "error" : ""}
            help={error?.subject_id ? error?.subject_id : ""}
          >
            <Select placeholder='Chọn môn' onChange={changeSubject}>
              {listSubject.map((subject) =>
                <Select.Option value={subject._id} key={subject._id}>{subject.name}</Select.Option>)}
            </Select>
          </Form.Item>
          <Form.Item
            name='part_id'
            label='Tên Dạng'
            validateStatus={error?.part_id ? "error" : ""}
            help={error?.part_id ? error?.part_id : ""}
          >
            <Select placeholder='Chọn dạng'>
              {listPart.map((part) =>
                <Select.Option value={part._id} key={part._id}>{part.name}</Select.Option>)}
            </Select>
          </Form.Item>
          <Form.Item
            name='name'
            label='Tên chủ đề'
            validateStatus={error?.name ? "error" : ""}
            help={error?.name ? error?.name : ""}
          >
            <Input style={{ width: 250 }} />
          </Form.Item>

        </Form>
      </Drawer>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.authState.user.user,
  };
};

export default connect(mapStateToProps)(DetailTopic);