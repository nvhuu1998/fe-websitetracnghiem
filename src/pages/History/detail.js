import React, { useEffect, useState } from 'react';
import { Button, Col, Drawer, notification, Popconfirm, Row, Space, Table, Form, Input, Select, Radio, Tree } from 'antd';
import Search from 'antd/lib/input/Search';
import Axios from 'axios';
import { connect } from "react-redux";
import dateFormat from 'dateformat'

const DetailHistory = (props) => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [total, setTotal] = useState();
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState('');
  const [perPage, setPerPage] = useState(10);
  const [visible, setVisibale] = useState(false);
  const [error, setError] = useState([]);
  const [loadPage, setLoadPage] = useState(true)
  const [dataEdit, setDataEdit] = useState();
  const [title, setTitle] = useState('')
  const [isEdit, setIsEdit] = useState(true)
  const [action, setAction] = useState('')
  const [listSubject, setListSubject] = useState([])
  const [type, setType] = useState(1)


  const columns = [
    {
      title: 'STT',
      dataIndex: 'key',
      key: 'key',
      render: (_, record) => (
        <Space>{(page - 1) * 10 + (data.indexOf(record) + 1)}</Space>
      )
    },
    {
      title: 'Người thi',
      dataIndex: 'user',
      key: 'user',
      render: (_, record) => (
        <Space>{record?.user?.name}</Space>
      )
    },
    {
      title: 'Môn',
      key: 'key',
      render: (_, record) => {
        return (
          <Space>{listSubject.filter(subject => subject._id = record?.exam_id.subject_id)[0]?.name}</Space>)
      }
    },
    {
      title: 'Ngày thi',
      dataIndex: 'date_create',
      key: 'date_create',
      render: (_, record) => (
        <Space>{dateFormat(record?.date_start, 'hh:MM:ssTT dd/mm/yyyy')}</Space>
      )
    },
    {
      title: 'Điểm',
      dataIndex: 'mark',
      key: 'key',
    },
    {
      title: 'Chức năng',
      key: 'key',
      render: (_, record) => (
        <Space>
          <Button type='primary' onClick={() => {
            setVisibale(true);
            setDataEdit(record);
          }}>
            Chi tiết</Button>
          <Popconfirm
            title='Xóa lịch sử này'
            okText='Xóa'
            cancelText='Hủy'
            onConfirm={() => deleteRecord(record._id)}>
            <Button type='primary' danger >Xóa</Button>
          </Popconfirm>
        </Space>
      )
    },
  ];


  const getData = async () => {
    let result;
    try {
      result = await Axios.post('/test', { search_text: search, page: page || 1, perPage: perPage })
      console.log(result?.data?.data)
      setData(result?.data?.data);
      setTotal(result?.data.total);
      const result2 = await Axios.get('subject')
      setListSubject(result2.data?.data)
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
    setLoading(false);
  }
  useEffect(() => {

    if (loading) {
      getData();
      setLoadPage(false);
    }
  }, [loading]);

  const handleChange = (pagination) => {
    setPage(pagination.current)
    setPerPage(pagination.pageSize)
    setLoading(true)
  }
  const closeModal = () => {
    setVisibale(false)
    setDataEdit([])
  }

  const deleteRecord = async (id) => {
    try {
      const result = await Axios.delete(`/exam/${id}`);
      setLoading(true)
      notification['success']({
        message: 'Thành công!',
        description: 'Xóa thành công!'
      })
    } catch (err) {
      notification['error']({
        message: 'Thất bại!',
        description: err.response?.data
      })
    }
  }



  return loadPage ? "" : (
    <div>
      <Row justify='space-between'>
        <Col><h2>Lịch sử thi</h2></Col>
      </Row>
      <br />
      <Table columns={columns}
        dataSource={data}
        size={'small'}
        rowKey='id'
        loading={loading}
        pagination={{ pageSize: perPage, current: page, total: total }}
        onChange={handleChange} />
      <Drawer
        title={title}
        width={500}
        onClose={closeModal}
        visible={visible}
        footer={
          <div
            style={{
              textAlign: 'left',
            }}
          >
            <Button onClick={closeModal} style={{ marginRight: 8 }}>
              Đóng
            </Button>

          </div>
        }
      >
        <Row>
          <Space>
            <span>Họ tên người thi: </span>
            <span>{dataEdit?.user?.name}</span>
          </Space>
        </Row>
        <Row>
          <Space>
            <span>Môn thi: </span>
            <span>{listSubject.filter(subject => subject._id = dataEdit?.exam_id?.subject_id)[0]?.name}</span>
          </Space>
        </Row>
        <Row>
          <Space>
            <span>Thời gian bắt đầu làm bài: </span>
            <span>{dateFormat(dataEdit?.date_start, 'hh:MM:ssTT dd/mm/yyyy')}</span>
          </Space>
        </Row>
        <Row>
          <Space>
            <span>Thời gian kết thúc: </span>
            <span>{dateFormat(dataEdit?.date_end, 'hh:MM:ssTT dd/mm/yyyy')}</span>
          </Space>
        </Row>
        <Row>
          <Space>
            <span>Số câu trả lời đúng: </span>
            <span>{dataEdit?.num_true}/{dataEdit?.answers?.length}</span>
          </Space>
        </Row>
        <Row>
          <Space>
            <span>Điểm: </span>
            <span>{dataEdit?.mark}</span>
          </Space>
        </Row>
      </Drawer>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.authState.user.user,
  };
};

export default connect(mapStateToProps,)(DetailHistory);