import React, { useEffect, useState } from 'react';
import { Button, Col, Drawer, notification, Popconfirm, Row, Space, Table, Form, Input, Select, Radio } from 'antd';
import Axios from 'axios';
import { connect } from "react-redux";
import ClassicEditor from "ckeditor5-mathtype/build/ckeditor";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import MyCustomUploadAdapterPlugin from '../../components/Myupload'
import { Upload, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

const DetailQues = (props) => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [total, setTotal] = useState();
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [visible, setVisibale] = useState(false);
  const [error, setError] = useState([]);
  const [loadPage, setLoadPage] = useState(true)
  const [dataEdit, setDataEdit] = useState();
  const [title, setTitle] = useState('')
  const [idSubject, setIdSubject] = useState('')
  const [idPart, setIdPart] = useState('')
  const [idTopic, setIdTopic] = useState('')
  const [action, setAction] = useState('')
  const [listSubject, setListSubject] = useState([])
  const [listPart, setListPart] = useState([])
  const [listTopic, setListTopic] = useState([])
  const [question, setQuestion] = useState()
  const [answer, setAnswer] = useState([])
  const [num, setnum] = useState()
  const [img, setImg] = useState('')
  const [count, setCount] = useState(4)
  const arr = new Array(count).fill(1)
  const radioStyle = {
    display: 'block',
    height: '30px',
    // lineHeight: auto,
  };
  const columns = [
    {
      title: 'STT',
      dataIndex: 'key',
      key: 'key',
      width: 100,
      render: (_, record) => (
        <Space>{(page - 1) * 10 + (data.indexOf(record) + 1)}</Space>
      )
    },
    {
      title: 'Tên câu hỏi',
      dataIndex: 'name',
      key: 'key',
    },
    {
      title: 'Tên Môn',
      dataIndex: 'nameSubject',
      key: 'key',
      render: (_, record) => <>{record?.subject_id?.name}</>
    },
    {
      title: 'Tên Dạng',
      dataIndex: 'namePart',
      key: 'key',
      render: (_, record) => <>{record?.part_id?.name}</>
    },
    {
      title: 'Tên chủ đề',
      dataIndex: 'nameTopic',
      key: 'key',
      render: (_, record) => <>{record?.topic_id?.name}</>
    },
    {
      title: 'Chức năng',
      key: 'key',
      render: (_, record) => (
        <Space>
          <Button type='primary' onClick={() => onClickEdit(record)}>
            Cập nhật</Button>
          <Popconfirm
            title='Xóa Chủ đề này'
            okText='Xóa'
            cancelText='Hủy'
            onConfirm={() => deleteRecord(record._id)}>
            <Button type='primary' danger >Xóa</Button>
          </Popconfirm>
        </Space>
      )
    },
  ];

  const getData = async () => {
    let result;
    try {
      if (idTopic !== '') {
        result = await Axios.post(`/ques/topic/${idTopic}`, { page: page || 1, perPage: perPage })
      }
      else
        if (idPart !== '') {
          result = await Axios.post(`/ques/part/${idPart}`, { page: page || 1, perPage: perPage })
          // let res = await Axios.get(`/topic/part/${idPart}`)
          // setListTopic(res?.data?.data)
        }
        else if (idSubject !== '') {
          result = await Axios.post(`/ques/subject/${idSubject}`, { page: page || 1, perPage: perPage })
        }
        else {
          result = await Axios.post('/ques', { page: page || 1, perPage: perPage })
          const result3 = await Axios.get('/part')
          setListPart(result3?.data?.data)
          const result2 = await Axios.post('/subject', { page: 1, perPage: 100 })
          setListSubject(result2?.data?.data)
          const res = await Axios.get('/topic')
          setListTopic(res?.data?.data)
        }
      setData(result?.data?.data);
      setTotal(result?.data.total);
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
    let result2;
    try {
      result2 = await Axios.post('/subject', { page: 1, perPage: 100 })
      setListSubject(result2?.data?.data)
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }

    setLoading(false);
  }
  useEffect(() => {
    if (loading) {
      getData();
      setLoadPage(false);
    }
  }, [loading]);

  const handleChange = (pagination) => {
    setPage(pagination.current)
    setPerPage(pagination.pageSize)
    setLoading(true)
  }

  const closeModal = () => {
    setError([])
    setVisibale(false)
    setDataEdit([])
  }
  const onClickEdit = (record) => {
    setImg(record.image)
    if (record.image)
      updateFileList([{
        uid: -1,
        name: record.image,
        status: 'done',
        url: `http://localhost:5000/public/${record.image}`
      }])
    setVisibale(true);
    setAction('Cập nhật');
    setDataEdit(record);
    setTitle('Cập nhật');
    setQuestion(record.question)
    let arr = []
    record.answers.forEach((ans, index) => {
      arr[index] = ans.ans
      if (ans.isTrue === true) setnum(index)
    });
    setAnswer(arr)
    form.setFieldsValue({
      name: record.name,
      subject_id: record.subject_id?._id,
      part_id: record.part_id?._id,
      topic_id: record.topic_id?._id
    })
    changeSubject(record.subject_id?._id)
  }
  const onCreate = async () => {
    let data
    try {
      data = await form.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    data.creator = props.user.id
    let answers = []
    for (let ans of answer) {
      let ans2 = { ans: ans, isTrue: false }
      answers.push(ans2)
    }
    answers[num] = {
      ...answers[num], isTrue: true
    }
    let formData = {
      subject_id: data.subject_id,
      part_id: data.part_id,
      topic_id: data.topic_id,
      creator: props.user.id,
      image: img,
      name: data.name,
      question: question,
      answers: answers
    }
    try {
      const result = await Axios.post('/ques/add', formData)
      if (result.status === 201) {
        setVisibale(false)
        setLoading(true)
        form.resetFields()
        setError([])
        setQuestion([])
        setnum('')
        setAnswer([])
        notification['success']({
          message: 'Thành công!',
          description: 'Thêm mới câu hỏi thành công!'
        })
      }
    } catch (err) {
      if (!err.response?.data) notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
      else
        setError(err.response?.data)
    }
  }
  const deleteRecord = async (id) => {
    try {
      const result = await Axios.delete(`/ques/${id}`);
      setLoading(true)
      notification['success']({
        message: 'Thành công!',
        description: 'Xóa thành công!'
      })
    } catch (err) {
      notification['error']({
        message: 'Thất bại!',
        description: err.response?.data
      })
    }
  }
  const [form] = Form.useForm()
  const onEdit = async () => {
    let data
    const id = dataEdit._id;
    try {
      data = await form.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    data.creator = props.user.id
    let answers = []
    for (let ans of answer) {
      let ans2 = { ans: ans, isTrue: false }
      answers.push(ans2)
    }
    answers[num] = {
      ...answers[num], isTrue: true
    }
    let formData = {
      subject_id: data.subject_id,
      part_id: data.part_id,
      topic_id: data.topic_id,
      creator: props.user.id,
      name: data.name,
      question: question,
      answers: answers
    }
    try {
      await Axios.patch(`/ques/${id}`, formData)
      setVisibale(false)
      setLoading(true)
      form.resetFields()
      setError([])
      setQuestion([])
      setnum('')
      setAnswer([])
      notification['success']({
        message: 'Thành công!',
        description: 'Cập nhật câu hỏi thành công!'
      })
    } catch (err) {
      if (!err.response?.data) notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
      else
        setError(err.response?.data)
    }
  }
  const changeSubject = async (value) => {
    let result;
    try {
      if (value !== '') {
        result = await Axios.get(`/part/subject/${value}`)
        const res = await Axios.get(`/topic/subject/${value}`)
        setListTopic(res?.data?.data)
      }
      else {
        result = await Axios.get(`/part`)
        const res = await Axios.get(`/topic`)
        setListTopic(res?.data?.data)
      }
      setListPart(result?.data?.data);
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
  }
  const changePart = async (value) => {
    let result;
    try {
      if (value !== '') {
        result = await Axios.get(`/topic/part/${value}`)
      }
      else {
        result = await Axios.get(`/topic`)
      }
      setListTopic(result?.data?.data);
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
  }

  const handleSubject = (value) => {
    setIdSubject(value)
    setIdPart('')
    changeSubject(value)
    setLoading(true)
    setPage(1)
  }
  const handlePart = (value) => {
    setIdPart(value)
    setPage(1)
    setLoading(true)
  }
  const handleTopic = (value) => {
    setIdTopic(value)
    setPage(1)
    setLoading(true)
  }
  const handleAnswer = (editor, index) => {
    let arr = answer;
    arr[index] = editor.getData()
    setAnswer(arr)
  }
  const changeRadio = (e) => {
    setnum(e.target.value)
  }

  const [fileList, updateFileList] = useState([]);
  const a = {
    name: 'image',
    multiple: false,
    action: 'http://localhost:5000/api/img',
    fileList,
    beforeUpload: file => {
      if (file.type !== 'image/png') {
        message.error(`${file.name} không phải là hình ảnh`);
      }
      return file.type === 'image/png';
    },
    onChange: async (info) => {
      const { status } = info.file;
      if (status === 'done') {
        message.success(`${info.file.name} đã được tải lên.`);
      } else if (status === 'error') {
        message.error(`${info.file.name} tải lên thất bại.`);
      }
      const files = info.fileList.filter(file => !!file.status)

      updateFileList([files[files.length - 1]]);
      setImg(files[files.length - 1])
    },
  };

  return loadPage ? "" : (
    <div>
      <Row justify='space-between'>
        <Col><h2>Danh sách câu hỏi</h2></Col>
        <Col ><Space>
          Môn:
          <Select defaultValue={idSubject} style={{ width: 200 }} onChange={handleSubject}>
            <Select.Option value=''>Tất cả</Select.Option>
            {listSubject.map((subject) =>
              <Select.Option value={subject._id} key={subject._id}>{subject.name}</Select.Option>)}
          </Select>
        </Space>
        </Col>
        <Col ><Space>
          Dạng:
          <Select value={idPart} style={{ width: 200 }} onChange={handlePart}>
            <Select.Option value=''>Tất cả</Select.Option>
            {listPart.map((part) =>
              <Select.Option value={part._id} key={part._id}>{part.name}</Select.Option>)}
          </Select>
        </Space>
        </Col>
        <Col ><Space>
          Chủ đề:
          <Select value={idTopic} style={{ width: 200 }} onChange={handleTopic}>
            <Select.Option value=''>Tất cả</Select.Option>
            {listTopic.map((topic) =>
              <Select.Option value={topic._id} key={topic._id}>{topic.name}</Select.Option>)}
          </Select>
        </Space>
        </Col>
        <Col>
          <Button type='primary'
            onClick={() => {
              setVisibale(true);
              setTitle('Thêm mới');
              setAction('Add');
              setImg(); updateFileList([])
              form.resetFields()
            }}>
            Thêm mới</Button>
        </Col>
      </Row>
      <br />
      <Table columns={columns}
        dataSource={data}
        size={'small'}
        rowKey='id'
        loading={loading}
        pagination={{ pageSize: perPage, current: page, total: total }}
        onChange={handleChange} />

      <Drawer
        title={title}
        width={750}
        onClose={closeModal}
        visible={visible}
        footer={
          <div
            style={{
              textAlign: 'left',
            }}
          >
            <Button onClick={closeModal} style={{ marginRight: 8 }}>
              Hủy
            </Button>
            {action === 'Add' ?
              <Button onClick={() => onCreate()} type="primary">
                Thêm
              </Button> : <Button onClick={() => onEdit()} type="primary">
                Cập nhật
              </Button>}

          </div>
        }
      >
        <Form
          form={form}
          layout="vertical"
        >
          <Row gutter={[16, 0]}>
            <Col span={12}>
              <Form.Item
                name='subject_id'
                label='Tên câu'
                validateStatus={error?.subject_id ? "error" : ""}
                help={error?.subject_id ? error?.subject_id : ""}
              >
                <Select placeholder='Chọn môn' onChange={changeSubject}>
                  {listSubject.map((subject) =>
                    <Select.Option value={subject._id} key={subject._id}>{subject.name}</Select.Option>)}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name='part_id'
                label='Tên Dạng'
                validateStatus={error?.part_id ? "error" : ""}
                help={error?.part_id ? error?.part_id : ""}
              >
                <Select placeholder='Chọn dạng' onChange={changePart}>
                  {listPart.map((part) =>
                    <Select.Option value={part._id} key={part._id}>{part.name}</Select.Option>)}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={[16, 0]}>
            <Col span={12}>
              <Form.Item
                name='topic_id'
                label='Tên chủ đề'
                validateStatus={error?.part_id ? "error" : ""}
                help={error?.part_id ? error?.part_id : ""}
              >
                <Select placeholder='Chọn chủ đề'>
                  {listTopic.map((topic) =>
                    <Select.Option value={topic._id} key={topic._id}>{topic.name}</Select.Option>)}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name='name'
                label='Tên Câu hỏi'
                validateStatus={error?.name ? "error" : ""}
                help={error?.name ? error?.name : ""}
              >
                <Input />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>

              <Form.Item
                name='question'
                label='Câu hỏi'
                validateStatus={error?.question ? "error" : ""}
                help={error?.question ? error?.question : ""}>
                <CKEditor editor={ClassicEditor}
                  data={question}
                  onChange={(e, edittor) => setQuestion(edittor.getData())}
                  config={
                    { extraPlugins: [MyCustomUploadAdapterPlugin] }
                    // { ckfinder: { uploadUrl: 'http://localhost:5000/api/img/upload' } }
                  }
                />
              </Form.Item>

            </Col>
          </Row>
          <Row>
            <Form.Item
              name='image'
              label='Hình ảnh'
              validateStatus={error?.name ? "error" : ""}
              help={error?.name ? error?.name : ""}
            >
              <Upload {...a}>
                <Button icon={<UploadOutlined />}>Upload png only</Button>
              </Upload>
            </Form.Item>
          </Row>
          <Radio.Group onChange={changeRadio} value={num}>
            {
              arr.map((value, index) =>
                <Row key={index} className='relative'>
                  <Radio value={index}>
                  </Radio>
                  <Form.Item
                    name='answer1'
                    label={`Câu trả lời ${index + 1}`}
                    validateStatus={error?.answers ? "error" : ""}
                    help={error?.answers ? error?.answers : ""}>

                    <CKEditor editor={ClassicEditor}
                      data={answer[index]}
                      onChange={(e, edittor) => handleAnswer(edittor, index)}
                    />
                  </Form.Item>
                </Row>)
            }
          </Radio.Group>
        </Form>
      </Drawer>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.authState.user.user,
  };
};

export default connect(mapStateToProps)(DetailQues);