import React, { useEffect, useState } from 'react';
import { Button, Col, Drawer, notification, Popconfirm, Row, Space, Table, Form, Input } from 'antd';
import Search from 'antd/lib/input/Search';
import Axios from 'axios';
import { connect } from "react-redux";

const DetailSubject = (props) => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [total, setTotal] = useState();
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState('');
  const [perPage, setPerPage] = useState(10);
  const [visible, setVisibale] = useState(false);
  const [error, setError] = useState([]);
  const [loadPage, setLoadPage] = useState(true)
  const [dataEdit, setDataEdit] = useState();
  const [title, setTitle] = useState('')
  const [isEdit, setIsEdit] = useState(true)
  const [action, setAction] = useState('')
  const columns = [
    {
      title: 'STT',
      dataIndex: 'key',
      key: 'key',
      width: 100,
      render: (_, record) => (
        <Space>{(page - 1) * 10 + (data.indexOf(record) + 1)}</Space>
      )
    },
    {
      title: 'Tên Môn',
      dataIndex: 'name',
      key: 'key',
    },
    // {
    //   title: 'Số dạng',
    //   dataIndex: 'countParts',
    //   key: 'countParts',
    // },
    {
      title: 'Chức năng',
      key: 'key',
      render: (_, record) => (
        <Space>
          <Button type='primary' onClick={() => {
            setVisibale(true);
            setAction('Cập nhật');
            setDataEdit(record);
            setTitle('Cập nhật');
            setIsEdit(false)
            form.setFieldsValue(record)
          }}>
            Cập nhật</Button>
          <Popconfirm
            title='Xóa Môn này'
            okText='Xóa'
            cancelText='Hủy'
            onConfirm={() => deleteRecord(record._id)}>
            <Button type='primary' danger >Xóa</Button>
          </Popconfirm>
        </Space>
      )
    },
  ];


  const getData = async () => {
    let result;
    try {
      result = await Axios.post('/subject', { search_text: search, page: page || 1, perPage: perPage })
      setData(result?.data?.data);
      setTotal(result?.data.total);
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
    setLoading(false);
  }
  useEffect(() => {
    if (loading) {
      getData();
      setLoadPage(false);
    }
  }, [loading]);

  const handleChange = (pagination) => {
    setPage(pagination.current)
    setPerPage(pagination.pageSize)
    setLoading(true)
  }
  const onSearch = value => {
    setSearch(value);
    setPage(1)
    setLoading(true);
  }
  const closeModal = () => {
    setError([])
    setVisibale(false)
    setDataEdit([])
    setIsEdit(true)
  }

  const onCreate = async () => {
    let data
    try {
      data = await form.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    data.creator = props.user.id
    try {
      const result = await Axios.post('/subject/add', data)
      if (result.status == 201) {
        setVisibale(false)
        setLoading(true)
        form.resetFields()
        setError([])
        notification['success']({
          message: 'Thành công!',
          description: 'Thêm mới Môn thành công!'
        })
      }
    } catch (err) {
      if (!err.response?.data) notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
      else
        setError(err.response?.data)
    }
  }
  const deleteRecord = async (id) => {
    try {
      await Axios.delete(`/subject/${id}`);
      setLoading(true)
      notification['success']({
        message: 'Thành công!',
        description: 'Xóa thành công!'
      })
    } catch (err) {
      notification['error']({
        message: 'Thất bại!',
        description: 'Lỗi sever'
      })
    }
  }
  const [form] = Form.useForm()
  const onEdit = async () => {
    let data
    const id = dataEdit._id;
    try {
      data = await form.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    data.creator = props.user.id
    try {
      await Axios.patch(`/subject/${id}`, data)
      setVisibale(false)
      setLoading(true)
      form.resetFields()
      setError([])
      notification['success']({
        message: 'Thành công!',
        description: 'Cập nhật Môn thành công!'
      })
    } catch (err) {
      if (!err.response?.data) notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
      else
        setError(err.response?.data)
    }
  }

  return loadPage ? "" : (
    <div>
      <Row justify='space-between'>
        <Col><h2>Danh sách Môn</h2></Col>
        <Col><Search
          placeholder="Nhập từ khóa"
          allowClear
          enterButton="Tìm kiếm"
          onSearch={onSearch}
        /></Col>
        <Col> <Button type='primary' onClick={() => { setVisibale(true); setTitle('Thêm mới'); setAction('Add'); form.resetFields() }}>
          Thêm mới</Button>
        </Col>
      </Row>
      <br />
      <Table columns={columns}
        dataSource={data}
        size={'small'}
        rowKey='id'
        loading={loading}
        pagination={{ pageSize: perPage, current: page, total: total }}
        onChange={handleChange} />

      <Drawer
        title={title}
        width={300}
        onClose={closeModal}
        visible={visible}
        footer={
          <div
            style={{
              textAlign: 'left',
            }}
          >
            <Button onClick={closeModal} style={{ marginRight: 8 }}>
              Hủy
            </Button>
            {action === 'Add' ?
              <Button onClick={() => onCreate()} type="primary">
                Thêm
              </Button> : <Button onClick={() => onEdit()} type="primary">
                Cập nhật
              </Button>}

          </div>
        }
      >
        <Form
          form={form}
          layout="vertical"
        >
          <Form.Item
            name='name'
            label='Tên môn'
            validateStatus={error?.name ? "error" : ""}
            help={error?.name ? error?.name : ""}
          >
            <Input style={{ width: 250 }} />
          </Form.Item>

        </Form>
      </Drawer>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.authState.user.user,
  };
};

export default connect(mapStateToProps,)(DetailSubject);