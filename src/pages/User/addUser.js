import React from 'react';
import { Form, Input } from 'antd';
import Modal from 'antd/lib/modal/Modal';
const AddUser = (props) => {
  const layout = {
    labelCol: {
      span: 8
    },
    wrapperCol: {
      span: 16
    }
  }

  const [form] = Form.useForm()
  return (
    <Modal
      visible={props.visible}
      title='Thêm user'
      // destroyOnClose={true}
      okText='Thêm'
      cancelText='Hủy'
      onCancel={() => {
        form.resetFields()
        props.closeModal()
      }}
      onOk={() => {
        form.validateFields()
          .then(values => {
            props.onCreate(values)
            if (props.visible === false) form.resetFields()
            // form.resetFields()
          })
          .catch(info => {
            console.log('Validate Failed:', info)
          })
      }}>
      <Form
        {...layout}
        form={form}
        name='nest-add'
        initialValues={{
          name: "",
          email: "",
          phone: "",
          password: '',
          password_confirm: ''
        }}
      >
        <Form.Item
          name='name'
          label='Họ và tên'
          validateStatus={props.error?.name ? "error" : ""}
          help={props.error?.name ? props.error?.name : ""}
        >
          <Input style={{ width: 250 }} />
        </Form.Item>
        <Form.Item
          name='email'
          label='Email'
          validateStatus={props.error?.email ? "error" : ""}
          help={props.error?.email ? props.error?.email : ""}
        >
          <Input style={{ width: 250 }} />
        </Form.Item>
        <Form.Item label='Số điện thoại' name='phone'>
          <Input
            style={{ width: 250 }}
            maxLength={10}
            onKeyPress={e => {
              if (!/^[0-9]/.test(e.key)) e.preventDefault()
            }}
          />
        </Form.Item>
        <Form.Item
          label='Mật khẩu'
          name='password'
          validateStatus={props.error?.password ? "error" : ""}
          help={props.error?.password ? props.error?.password : ""}
        >
          <Input.Password style={{ width: 250 }} autoComplete='on' />
        </Form.Item>
        <Form.Item
          label='Nhập lại mật khẩu'
          name='password_confirm'
          validateStatus={props.error?.password_confirm ? "error" : ""}
          help={props.error?.password_confirm ? props.error?.password_confirm : ""}
        >
          <Input.Password style={{ width: 250 }} autoComplete='on' />
        </Form.Item>
      </Form>
    </Modal>
  )
}
export default AddUser