import React, { useEffect, useState } from 'react';
import { Button, Col, Drawer, notification, Popconfirm, Row, Space, Table, Form, Input, Select } from 'antd';
import Search from 'antd/lib/input/Search';
import Axios from 'axios';



const DetailUser = () => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [total, setTotal] = useState();
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState('');
  const [perPage, setPerPage] = useState(10);
  const [visible, setVisibale] = useState(false);
  const [error, setError] = useState([]);
  const [loadPage, setLoadPage] = useState(true)
  const [dataEdit, setDataEdit] = useState();
  const [title, setTitle] = useState('')
  const [isEdit, setIsEdit] = useState(true)
  const [action, setAction] = useState('')
  const columns = [
    {
      title: 'STT',
      dataIndex: 'key',
      key: 'key',
      render: (_, record) => (
        <Space>{(page - 1) * 10 + (data.indexOf(record) + 1)}</Space>
      )
    },
    {
      title: 'Tên người dùng',
      dataIndex: 'name',
      key: 'key',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'key',
    },
    {
      title: 'Quyền',
      dataIndex: 'role',
      key: 'key',
      render: (_, record) => (<Space>{record.role === 1 ? 'Admin' : 'Khách'}</Space>)

    },
    {
      title: 'Chức năng',
      key: 'key',
      render: (_, record) => (
        <Space>
          <Button type='primary' onClick={() => {
            setVisibale(true);
            setAction('Chi tiết');
            setDataEdit(record);
            setTitle('Chi tiết');
            setIsEdit(false)
            form.setFieldsValue(record)
          }}>
            Chi tiết</Button>
          <Popconfirm
            title='Xóa user này'
            okText='Xóa'
            cancelText='Hủy'
            onConfirm={() => deleteRecord(record._id)}>
            <Button type='primary' danger >Xóa</Button>
          </Popconfirm>
        </Space>
      )
    },
  ];


  const getData = async () => {
    let result;
    try {
      result = await Axios.post('/users', { search_text: search, page: page || 1, perPage: perPage })
      setData(result?.data?.data);
      setTotal(result?.data.total);
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
    setLoading(false);
  }
  useEffect(() => {
    if (loading) {
      getData();
      setLoadPage(false);
    }
  }, [loading]);

  const handleChange = (pagination) => {
    setPage(pagination.current)
    setPerPage(pagination.pageSize)
    setLoading(true)
  }
  const onSearch = value => {
    setSearch(value);
    setPage(1)
    setLoading(true);
  }
  const closeModal = () => {
    setError([])
    setVisibale(false)
    setDataEdit([])
    setIsEdit(true)
  }

  const onCreate = async () => {
    let data
    try {
      data = await form.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    try {
      Axios.post('users/register', data)
      setVisibale(false)
      setLoading(true)
      form.resetFields()
      setError([])
      notification['success']({
        message: 'Thành công!',
        description: 'Thêm mới user thành công!'
      })
    } catch (err) {
      if (!err.response?.data) notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
      else
        setError(err.response?.data)
    }
  }
  const deleteRecord = async (id) => {
    try {
      const result = await Axios.delete(`/users/${id}`);
      setLoading(true)
      notification['success']({
        title: 'Thành công!',
        message: 'Xóa user thành công!'
      })
    } catch (err) {
      notification['error']({
        title: 'Thất bại!',
        message: err.response.data
      })
    }
  }
  const [form] = Form.useForm()
  const onEdit = async () => {
    let data
    const id = dataEdit._id;
    try {
      data = await form.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    try {
      await Axios.patch(`/users/${id}`, data)
      setVisibale(false)
      setLoading(true)
      form.resetFields()
      setError([])
      notification['success']({
        message: 'Thành công!',
        description: 'Cập nhật user thành công!'
      })
    } catch (err) {
      if (!err.response?.data) notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
      else
        setError(err.response?.data)
    }
  }
  return loadPage ? "" : (
    <div>
      <Row justify='space-between'>
        <Col><h2>Danh sách người dùng</h2></Col>
        <Col><Search
          placeholder="Nhập từ khóa"
          allowClear
          enterButton="Tìm kiếm"
          onSearch={onSearch}
        /></Col>
        <Col> <Button type='primary' onClick={() => { setVisibale(true); setTitle('Thêm mới'); setAction('Add'); form.resetFields() }}>
          Thêm mới</Button>
        </Col>
      </Row>
      <br />
      <Table columns={columns}
        dataSource={data}
        size={'small'}
        rowKey='id'
        loading={loading}
        pagination={{ pageSize: perPage, current: page, total: total }}
        onChange={handleChange} />

      <Drawer
        title={title}
        width={300}
        onClose={closeModal}
        visible={visible}
        footer={
          <div
            style={{
              textAlign: 'left',
            }}
          >
            <Button onClick={closeModal} style={{ marginRight: 8 }}>
              Hủy
            </Button>
            {action === 'Chi tiết' ?
              <Button onClick={() => { setIsEdit(true); setTitle('Cập nhật'); setAction('Cập nhật') }} type="primary">
                Chỉnh sửa
              </Button> : action === 'Add' ?
                <Button onClick={() => onCreate()} type="primary">
                  Thêm
              </Button> : <Button onClick={() => onEdit()} type="primary">
                  Cập nhật
              </Button>}

          </div>
        }
      >
        <Form
          form={form}
          layout="vertical"
        >
          <Form.Item
            name='name'
            label='Họ và tên'
            validateStatus={error?.name ? "error" : ""}
            help={error?.name ? error?.name : ""}
          >
            <Input style={{ width: 250 }} disabled={!isEdit} />
          </Form.Item>
          <Form.Item
            name='email'
            label='Email'
            validateStatus={error?.email ? "error" : ""}
            help={error?.email ? error?.email : ""}
          >
            <Input style={{ width: 250 }} disabled={action === 'Add' ? false : true} />
          </Form.Item>
          <Form.Item label='Số điện thoại' name='phone'>
            <Input
              style={{ width: 250 }}
              maxLength={10}
              onKeyPress={e => {
                if (!/^[0-9]/.test(e.key)) e.preventDefault()
              }}
              disabled={!isEdit}
            />
          </Form.Item>
          {action === 'Add' ? <><Form.Item
            label='Mật khẩu'
            name='password'
            validateStatus={error?.password ? "error" : ""}
            help={error?.password ? error?.password : ""}
          >
            <Input.Password style={{ width: 250 }} autoComplete='on' />
          </Form.Item>
            <Form.Item
              label='Nhập lại mật khẩu'
              name='password_confirm'
              validateStatus={error?.password_confirm ? "error" : ""}
              help={error?.password_confirm ? error?.password_confirm : ""}
            >
              <Input.Password style={{ width: 250 }} autoComplete='on' />
            </Form.Item></> : ""}
          <Form.Item
            label='Phân quyền'
            name='role'
            validateStatus={error?.role ? "error" : ""}
            help={error?.role ? error?.role : ""}
          >
            <Select value={2} disabled={!isEdit} >
              <Select.Option value={1} >Admin</Select.Option>
              <Select.Option value={2} >Khách</Select.Option>
            </Select>
          </Form.Item>
        </Form>
      </Drawer>
    </div>
  )
}
export default DetailUser;