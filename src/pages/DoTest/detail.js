import { Col, Row, notification, Button, Radio, Result, Modal } from 'antd';
import { connect } from "react-redux";
import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import HtmlParser from 'react-html-parser';
import { useHistory } from "react-router";
import dateFormat from 'dateformat'

const URL = 'http:/localhost:5000/public/'
const radioStyle = {
  display: 'block',

};
const DetailDoTest = (props) => {
  const history = useHistory();
  const [data, setData] = useState()
  const [loading, setLoading] = useState(true)
  const [answers, setAnswers] = useState([])
  const [list, setList] = useState([])
  const [title, setTitle] = useState('')
  const [visible, setVisible] = useState(false)
  const [visible2, setVisible2] = useState(false)
  const [ketqua, setKetqua] = useState()
  const [date_start, setDate_start] = useState()



  const getData = async () => {

    try {
      const result = await Axios.get(`/exam/${props.match.params.id}`)
      setData(result?.data?.data)
      var foo = [];
      for (var i = 1; i <= result?.data?.data?.ques?.length; i++) {
        foo.push('');
      }
      setAnswers(foo)
      setDate_start(new Date())
    } catch (error) {
      notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
    }
  }
  const changeAns = (e, index) => {
    let ans = answers
    ans[index] = e.target.value
    setAnswers(ans)
  }
  useEffect(() => {
    getData()
    setLoading(false)

  }, [])
  const submitTest = async () => {
    let formData = {
      user: props.user,
      exam_id: props.idExam,
      answers,
      date_start: dateFormat(date_start, 'isoUtcDateTime')
    }
    try {
      const result = await Axios.post('/test/add', formData)
      notification['success']({
        message: 'Thành công!',
        description: 'Nộp bài thành công!'
      })
      setKetqua(result?.data?.data)
      setVisible2(true)

    } catch (error) {
      notification['error']({
        message: 'Lỗi!',
        description: 'Nộp bài thất bại! Vui lòng thử lại'
      })
    }
  }
  const showModel = () => {
    setVisible(true)
    let arr = []
    answers.forEach((ans, index) => {
      if (ans === '') arr.push(index + 1)
    })
    setList(arr)
    if (arr.length !== 0) {
      setTitle(`Các câu chưa hoàn thành: ${arr.join(', ')} ! 
        Vẫn tiếp tục nộp bài?`)
    } else {
      setTitle("Chọn Ok để nộp bài!")
    }

  }
  const handleOk = () => {
    submitTest()
    setVisible(false);
  };

  const handleCancel = () => {
    setVisible(false);
  };
  const changePage = () => {
    setVisible2(false)
    history.push('/historytest')
  }
  return loading ? '' : (
    <Row align='middle' justify='center' style={{ minHeight: '80vh', margin: '20px' }} gutter={16}>
      <Col span={18} offset={2}>
        {data?.ques.map((ques, index) => (<>
          <Row key={index}>
            <strong> Câu {index + 1}:</strong>{HtmlParser(ques.question_id.question)}
          </Row>

          {ques.question_id.image ? <Row><image src={`${URL}${ques.question_id.image}`} /> </Row> : ''}
          < Row >
            <Radio.Group onChange={(e) => changeAns(e, index)} defaultValue=''>
              <Radio style={radioStyle} value={ques?.question_id?.answers[0]?._id}>
                A. <span style={{ display: 'inline-block' }}>  {HtmlParser(ques.question_id.answers[0]?.ans)}</span>
              </Radio>

              <Radio style={radioStyle} value={ques.question_id?.answers[1]?._id}>
                B. <span style={{ display: 'inline-block' }}>  {HtmlParser(ques.question_id.answers[1]?.ans)}</span>
              </Radio>
              <Radio style={radioStyle} value={ques.question_id?.answers[2]?._id}>
                C. <span style={{ display: 'inline-block' }}>  {HtmlParser(ques.question_id.answers[2]?.ans)}</span>
              </Radio>
              <Radio style={radioStyle} value={ques.question_id?.answers[3]?._id}>
                D. <span style={{ display: 'inline-block' }}>  {HtmlParser(ques.question_id.answers[3]?.ans)}</span>
              </Radio>
            </Radio.Group>

          </Row></>)
        )}
        {!data ?
          <Result
            title="Chưa có đề thi, vui lòng chọn lại!"
            extra={
              <Button type="primary" key="console" onClick={() => history.push('/public')}>
                Tới trang chủ
              </Button>
            }
          />
          :
          <Row>
            <Col offset={10}>
              <Button type='primary' onClick={() => showModel()}>Nộp bài</Button>
            </Col>
          </Row>}

      </Col>
      <Modal
        title="Xác nhận nộp bài thi"
        visible={visible}
        onOk={() => handleOk()}
        onCancel={() => handleCancel()}
      >
        <p>{title}</p>
      </Modal>
      <Modal
        title="Kết quả thi"
        visible={visible2}
        onOk={() => changePage()}
        onCancel={() => changePage()}
      >
        <p>Số câu trả lời đúng: {ketqua?.num_true}/{ketqua?.answers?.length}</p>
        <p>Điểm: {ketqua?.mark}</p>
      </Modal>
    </Row >

  )
}
const mapStateToProps = (state) => {
  return {
    user: state.authState.user.user.id,
    idExam: state.idExam
  };
};

export default connect(mapStateToProps,)(DetailDoTest);