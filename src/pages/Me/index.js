import React, { useEffect, useState } from 'react';
import { Button, Col, Drawer, notification, Popconfirm, Row, Space, Table, Form, Input } from 'antd';
import Search from 'antd/lib/input/Search';
import Axios from 'axios';
import { connect } from "react-redux";

const MeDetail = (props) => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState('');
  const [perPage, setPerPage] = useState(10);
  const [visible, setVisibale] = useState(false);
  const [error, setError] = useState([]);
  const [loadPage, setLoadPage] = useState(true)
  const [dataEdit, setDataEdit] = useState();
  const [title, setTitle] = useState('')

  const getData = async () => {
    let result;
    try {
      result = await Axios.get('/users/me')
      console.log(result)
      setData(result?.data?.data);
    } catch (err) {
      notification['error']({
        description: 'Lỗi!',
        message: "Lỗi sever"
      })
    }
    setLoading(false);
  }
  useEffect(() => {
    if (loading) {
      getData();
      setLoadPage(false);
    }
  }, [loading]);


  const closeModal = () => {
    setError([])
    setVisibale(false)
    setDataEdit([])
    setIsEdit(true)
  }


  const [form] = Form.useForm()
  const onEdit = async () => {
    let data
    const id = dataEdit._id;
    try {
      data = await form.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    data.creator = props.user.id
    try {
      await Axios.patch(`/users/${id}`, data)
      setVisibale(false)
      setLoading(true)
      form.resetFields()
      setError([])
      notification['success']({
        message: 'Thành công!',
        description: 'Cập nhật Môn thành công!'
      })
    } catch (err) {
      if (!err.response?.data) notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
      else
        setError(err.response?.data)
    }
  }

  return loadPage ? "" : (
    <div>
      <Row justify='space-between'>
        <Col><h2>Thông tin tài khoản</h2></Col>
        <Col> <Button type='primary' onClick={() => { setVisibale(true); setTitle('Cập nhật'); setAction('edit'); form.resetFields(data) }}>
          Cập nhật</Button>
        </Col>
      </Row>
      <br />
      <Drawer
        title={title}
        width={300}
        onClose={closeModal}
        visible={visible}
        footer={
          <div
            style={{
              textAlign: 'left',
            }}
          >
            <Button onClick={closeModal} style={{ marginRight: 8 }}>
              Hủy
            </Button>
            <Button onClick={() => onEdit()} type="primary">
              Cập nhật
              </Button>

          </div>
        }
      >
        <Form
          form={form}
          layout="vertical"
        >
          <Form.Item
            name='name'
            label='Họ và tên'
            validateStatus={error?.name ? "error" : ""}
            help={error?.name ? error?.name : ""}
          >
            <Input style={{ width: 250 }} />
          </Form.Item>
          <Form.Item
            name='email'
            label='Email'
            validateStatus={error?.email ? "error" : ""}
            help={error?.email ? error?.email : ""}
          >
            <Input style={{ width: 250 }} />
          </Form.Item>
          <Form.Item label='Số điện thoại' name='phone'>
            <Input
              style={{ width: 250 }}
              maxLength={10}
              onKeyPress={e => {
                if (!/^[0-9]/.test(e.key)) e.preventDefault()
              }}

            />
          </Form.Item>
        </Form>
      </Drawer>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.authState.user.user,
  };
};

export default connect(mapStateToProps,)(MeDetail);