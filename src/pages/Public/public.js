import { Card, Form, Row, Select, Radio, Space, Tree, Button, notification } from "antd";
import React, { useEffect, useState } from "react";
import { connect, useDispatch } from "react-redux";
import Axios from 'axios';
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { TestType } from "../../redux/actions/AuthAction";
const Public = () => {
  const history = useHistory();
  const dispatch = useDispatch()
  const [error, setError] = useState([])
  const [listSubject, setListSubject] = useState([])
  const [form] = Form.useForm()
  const [random, setRandom] = useState(1)
  const [idSubject, setIdsubject] = useState('')
  const [idExam, setIdExam] = useState()

  const getData = async () => {
    try {
      const result = await Axios.get('/subject')
      setListSubject(result?.data?.data)
    } catch (error) {

    }
  }
  useEffect(() => {
    getData()
  }, []);
  const changeSubject = async (value) => {
    setIdsubject(value)
    try {
      const result = await Axios.get(`/part/subject/${value}`)
      const tree = result.data?.data
      let treeDataa = []
      tree.map(part => {
        let child = []
        part.topics.map(topic => {
          child.push({ title: topic.name, key: topic._id })
        })
        treeDataa.push({ title: part.name, key: part._id, children: child })
      })
      setTreeData(treeDataa)
    } catch (error) {

    }
  }
  const getExam = async () => {
    let result
    if (!idSubject) setError({ subject_id: 'Vui lòng chọn môn' })
    else
      try {
        if (!random) setError({ random: 'Vui lòng chọn dạng đề thi' })
        else
          if (random === 1) {
            result = await Axios.post('/exam/add', { subject_id: idSubject })
          }
          else if (random === 2) {
            result = await Axios.post('/exam/add/topic', { subject_id: idSubject, topics: checkedKeys })
          }
        setIdExam(result?.data?.data?._id)
        dispatch({ type: TestType.IDTEST_SUCCESS, payload: result?.data?.data?._id })
        history.push(`/dotest/${result?.data?.data?._id}`)

      } catch (error) {
        notification['error']({
          description: 'Lỗi!',
          message: "Lỗi sever"
        })
        // dispatch({ type: TestType.IDTEST_SUCCESS, payload: idExam })
      }
  }
  const [treeData, setTreeData] = useState([])
  const [expandedKeys, setExpandedKeys] = useState([]);
  const [checkedKeys, setCheckedKeys] = useState([]);
  const [selectedKeys, setSelectedKeys] = useState([]);
  const [autoExpandParent, setAutoExpandParent] = useState(true);

  const onExpand = (expandedKeys) => {
    setExpandedKeys(expandedKeys);
    setAutoExpandParent(false);
  };

  const onCheck = (checkedKeys) => {
    setCheckedKeys(checkedKeys);
  };

  const onSelect = (selectedKeys, info) => {
    setSelectedKeys(selectedKeys);
  };

  return (
    <Row align='middle' justify='center' style={{ minHeight: '80vh', margin: '20px' }} >
      <Card title='Đề thi trắc nghiệm' bordered={true} style={{ width: 800 }} className='card-public'>
        <Form
          form={form}
          layout="vertical"
        >
          <Form.Item
            name='subject_id'
            label='Tên Môn'
            validateStatus={error?.subject_id ? "error" : ""}
            help={error?.subject_id ? error?.subject_id : ""}
          >
            <Select placeholder='Chọn môn' onChange={changeSubject}>
              {listSubject.map((subject) =>
                <Select.Option value={subject._id} key={subject._id}>{subject.name}</Select.Option>)}
            </Select>
          </Form.Item>

          <Form.Item
            name='random'
            label='Chọn dạng đề thi'
            validateStatus={error?.random ? "error" : ""}
            help={error?.random ? error?.random : ""}
          >
            <Radio.Group defaultValue={random} buttonStyle="solid" onChange={(e) => setRandom(e.target.value)}>
              <Space>
                <Radio.Button value={1}>Trộn đề ngẫu nhiên</Radio.Button>
                <Radio.Button value={2}>Trộn đề tự chọn</Radio.Button>
              </Space>
            </Radio.Group>
          </Form.Item>
          {random === 2 ? <Form.Item
            label='Chọn chủ đề'
          // validateStatus={error?.subject_id ? "error" : ""}
          // help={error?.subject_id ? error?.subject_id : ""}
          >
            <Tree
              checkable
              onExpand={onExpand}
              expandedKeys={expandedKeys}
              autoExpandParent={autoExpandParent}
              onCheck={onCheck}
              checkedKeys={checkedKeys}
              onSelect={onSelect}
              selectedKeys={selectedKeys}
              treeData={treeData}
            />
          </Form.Item> : ''}
          <Button type='primary' onClick={() => getExam()}>Làm bài thi</Button>
        </Form>
      </Card>
    </Row>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.authState.user.user,
  };
};

export default connect(mapStateToProps)(Public);