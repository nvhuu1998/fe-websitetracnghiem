import React, { useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router";
import { AuthActionType } from '../../redux/actions/AuthAction';
import Axios from "axios";
import 'antd/dist/antd.css';
import { Form, Input, Button, Checkbox, Col, Row, Card, notification } from 'antd';
import Layout, { Content, Header } from "antd/lib/layout/layout";
import FooterPage from "../../components/Footer";
import { useDispatch } from 'react-redux'
const layout = {
  labelCol: {
    span: 10,
  },
  wrapperCol: {
    span: 14,
  },
};
const layout2 = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 10,
    span: 14,
  },
};

const Login = (props) => {

  const dispatch = useDispatch()
  const [error, setError] = useState([])
  const [error2, setError2] = useState([])
  const history = useHistory();
  const [form] = Form.useForm()
  const [form2] = Form.useForm()
  const login = async () => {
    let data2
    try {
      data2 = await form.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    try {
      const res = await Axios.post("/users/login", data2);
      const { data } = res;
      dispatch({ type: AuthActionType.LOGIN_SUCCESS, payload: data });
      history.push("/public");
    } catch (error) {
      if (error.response) {
        setError(error.response.data)
      }
    }
  }
  const Register = async () => {
    let data
    try {
      data = await form2.validateFields()
    } catch (info) {
      console.log('Validate Failed:', info);
    }
    try {
      Axios.post('users/register', data)
      form2.resetFields()
      setError2([])
      notification['success']({
        message: 'Thành công!',
        description: 'Thêm mới user thành công, đăng nhập để tiếp tục!'
      })
    } catch (err) {
      if (!err.response?.data) notification['error']({
        message: 'Lỗi!',
        description: 'Lỗi sever!'
      })
      else
        setError2(err.response?.data)
    }
  }
  return (
    <Layout >
      <Header><h1 className='header-center'>Website hỗ trợ ôn thi đại học</h1></Header>
      <Content style={{ minHeight: "69vh", margin: '50px' }}>

        <Row>
          <Col span={9} offset={2}>

            <Card title='Đăng nhập'>
              <Form
                form={form}
                // {...layout2}
                layout='vertical'
                name="basic"
              >
                <Form.Item
                  label="Email"
                  name="email"
                  validateStatus={error?.email ? "error" : ""}
                  help={error?.email ? error?.email : ""}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Password"
                  name="password"
                  validateStatus={error?.password ? "error" : ""}
                  help={error?.password ? error?.password : ""}
                >
                  <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout} >
                  <Button type="primary" htmlType="submit" onClick={login}>
                    Đăng nhập
                  </Button>
                </Form.Item>
              </Form>
            </Card>
          </Col>
          <Col span={9} offset={2}>
            <Card title='Đăng ký tài khoản'>
              <Form
                form={form2}
                {...layout}
                name="basic"
              >
                <Form.Item
                  name='name'
                  label='Họ và tên'
                  validateStatus={error2?.name ? "error" : ""}
                  help={error2?.name ? error2?.name : ""}
                >
                  <Input style={{ width: 250 }} />
                </Form.Item>
                <Form.Item
                  name='email'
                  label='Email'
                  validateStatus={error2?.email ? "error" : ""}
                  help={error2?.email ? error2?.email : ""}
                >
                  <Input style={{ width: 250 }} />
                </Form.Item>
                <Form.Item label='Số điện thoại' name='phone'>
                  <Input
                    style={{ width: 250 }}
                    maxLength={10}
                    onKeyPress={e => {
                      if (!/^[0-9]/.test(e.key)) e.preventDefault()
                    }}

                  />
                </Form.Item>
                <Form.Item
                  label='Mật khẩu'
                  name='password'
                  validateStatus={error2?.password ? "error" : ""}
                  help={error2?.password ? error2?.password : ""}
                >
                  <Input.Password style={{ width: 250 }} autoComplete='on' />
                </Form.Item>
                <Form.Item
                  label='Nhập lại mật khẩu'
                  name='password_confirm'
                  validateStatus={error?.password_confirm ? "error" : ""}
                  help={error?.password_confirm ? error?.password_confirm : ""}
                >
                  <Input.Password style={{ width: 250 }} autoComplete='on' />
                </Form.Item>
                <Form.Item {...tailLayout}>
                  <Button type="primary" onClick={Register}>
                    Đăng ký
                  </Button>
                </Form.Item>
              </Form>
            </Card>
          </Col>
        </Row>
      </Content>
      <FooterPage />
    </Layout>

  );
};
const mapStateToProps = (state) => {
  return {
    user: state,
  };
};

export default connect(mapStateToProps)(Login);